#!/usr/bin/env python3

import os
# import os.path
# from PIL import Image
import matplotlib.pyplot as plt # todo try import
import dhondt, saintelague, adams, anspruch, hareniemeyer
import sperr


def pieplot(sizes, colors, labels, title, outfile):
    """
    plot a pie chart from given arguments
    """
    patches = plt.pie(sizes, colors=colors, shadow=False, startangle=0)[0]
    plt.legend(patches, labels) # , loc="best")
    plt.axis('equal')
    plt.title(title)
    plt.savefig("assets/"+outfile, dpi=160)
    return


def barplot(sizes, colors, labels, ylabel, title, outfile, trans=False):
    """
    plot a bar chart from given arguments with multiple categories
    """
    if trans:
        # transpose data
        sizes = [[sizes[j][i] for j in range(len(sizes))] for i in range(len(sizes[0]))]
    ind = [i for i in range(len(sizes[0]))]
    fig, ax = plt.subplots()
    bar_width = .9/len(sizes)
    for i in range(len(sizes)):
        if trans:
            # fix colors for transposed data
            ax.bar([w+i*bar_width for w in ind], [s for s in sizes[i]], bar_width, color=colors[i], label=labels)
        else:
            ax.bar([w+i*bar_width for w in ind], [s for s in sizes[i]], bar_width, color=colors, alpha=1-(i%2)*.3, label=labels)
    # plt.xlabel('Gruppen')
    plt.ylabel(ylabel)
    plt.title(title)
    plt.xticks([w+bar_width for w in ind], labels)
    plt.savefig("assets/"+outfile, dpi=160)
    return


def tupelplot(sizes, labels, ylabel, title, outfile):
    """
    plot a bar chart from given arguments with a single category
    """
    fig, ax = plt.subplots()
    bar_width = .9
    ax.bar([w for w in range(len(sizes))], sizes, bar_width, color=["#ff9900", "#00ff99"], label=labels)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.xticks([step for step in range(len(sizes))], labels)
    plt.savefig("assets/"+outfile, dpi=160)
    return

def output(data, h3=""):
    if h3:
        print("\n### "+h3+"\n")
    for d in data:
        print("* {}: {}".format(d[0], d[1]))
    return


def main():
    # todo set plot and verbose value from input
    plot = True # plot data and save to file?
    verbose = 2 # output data to stdout
    parlamentsgroesse = 61
    fraktionen = readwahlfile("wahlergebnis")
    """
    fraktionen = [
        ("ADF", 2673, "#6666ff"),
        ("RCDS", 601, "#0000ff"),
        ("LHG", 376, "#ff00ff"),
        ("GHG", 1374, "#33ff33"),
        ("JuSo", 945, "#ff0000"),
        ("ALL", 781, "#cc00cc"),
        ("NeLi", 401, "#ffcc00"),
        ("LISTE", 169, "#ff9999"),
        ("SRK", 116, "#333333"),
        ("PARTEI", 114, "#999999")
    ]
    """
    colors = [c[2] for c in fraktionen]
    colors.append("#00000000") # hack to draw transparent half circle
    # summestimmen = sum([s[1] for s in fraktionen])
    
    # Sitzverteilungen
    ## Anspruch
    sitze_anspruch = anspruch.verteil(parlamentsgroesse, fraktionen)
    
    if verbose > 0:
        print("# HondtWechsel\n")
        # todo print md to display logo
        print("## Sitzverteilungen")
        output(sitze_anspruch, "Anspruch")
    
    if plot:
        labels = [x[0]+" ({0:.1f})".format(x[1]) for x in sitze_anspruch]
        labels.append("Gesamt: {}".format(parlamentsgroesse)) # hack to draw only half circle
        sizes = [x[1] for x in sitze_anspruch]
        sizes.append(parlamentsgroesse) # hack to draw only half circle
        pieplot(sizes, colors, labels, "Sitzanspruch nach Wahlergebnis", "anspruch.png")
    
    ## DHondt
    sitze_dhondt = dhondt.verteil(parlamentsgroesse, fraktionen)
    
    if verbose > 0:
        output(sitze_dhondt, "D’Hondt")
    
    # hha
    """
    zg = [        
        ('ADF', 23),
        ('RCDS', 5),
        ('LHG', 3),
        ('ghg', 11),
        ('juso', 8),
        ('all', 6),
        ('nerd', 3),
        ('liste', 1),
        ('srk', 1),
        ('partei', 0)
    ]
    kommission = adams.verteil(7, zg)
    print("## Kommissionen")
    for k in kommission:
        print(" ", k)
    """
    
    if plot:
        labels = [x[0]+" ({})".format(x[1]) for x in sitze_dhondt]
        labels.append("Gesamt: {}".format(parlamentsgroesse))
        sizes = [x[1] for x in sitze_dhondt]
        sizes.append(parlamentsgroesse)
        pieplot(sizes, colors, labels, "Sitzverteilung nach D’Hondt", "dhondt.png")
    
    ## Sainte-Lague
    sitze_sainte = saintelague.verteil(parlamentsgroesse, fraktionen)
    
    if verbose > 0:
        output(sitze_sainte, "Sainte-Laguë")
    
    if plot:
        labels = [x[0]+" ({})".format(x[1]) for x in sitze_sainte]
        labels.append("Gesamt: {}".format(parlamentsgroesse))
        sizes = [x[1] for x in sitze_sainte]
        sizes.append(parlamentsgroesse)
        pieplot(sizes, colors, labels, "Sitzverteilung nach Sainte-Laguë", "sainte.png")

    ## Adams
    sitze_adams = adams.verteil(parlamentsgroesse, fraktionen)
    
    if verbose > 0:
        output(sitze_adams, "Adams")
    
    if plot:
        labels = [x[0]+" ({})".format(x[1]) for x in sitze_adams]
        labels.append("Gesamt: {}".format(parlamentsgroesse))
        sizes = [x[1] for x in sitze_adams]
        sizes.append(parlamentsgroesse)
        pieplot(sizes, colors, labels, "Sitzverteilung nach Adams", "adams.png")
        
    # Analyse
    colors.pop() # remove hack color
    labels = [f[0] for f in fraktionen]
    
    ## Vergleich Sitze nach Verfahren
    data = []
    data.append([s[1] for s in sitze_anspruch])
    data.append([s[1] for s in sitze_dhondt])
    data.append([s[1] for s in sitze_sainte])
    
    if plot:
        barplot(data, colors, labels, "Sitze", "Vergleich Sitze Anspruch/D’Hondt/Sainte-Laguë", "vergleich.png")
    
    ## Sitze pro 100 Stimmen
    for i in range(len(data[0])):
        for j in range(len(data)):
            data[j][i] /= 0.01 * fraktionen[i][1]
    
    if verbose > 1:
        print("\n## Sitze pro 100 Stimmen")
        output(list(zip(labels, data[1])), "D’Hondt")
        output(list(zip(labels, data[2])), "Sainte-Laguë")
    
    if plot:
        barplot(data, colors, ["Anspruch","D’Hondt","Sainte-Laguë"], "Sitze", "Sitze pro 100 Stimmen", "quote.png", trans=True)
    
    ## Differenz von Sitzen pro 100 Stimmen
    for i in range(len(data[0])):
        data[1][i] = abs(data[0][i] - data[1][i])
        data[2][i] = abs(data[0][i] - data[2][i])
    
    if verbose > 1:
        print("\n## Differenz zum Sitzanspruch pro 100 Stimmen")
        output(list(zip(labels, data[1])), "D’Hondt")
        output(list(zip(labels, data[2])), "Sainte-Laguë")
    
    if plot:
        barplot([data[1],data[2]], colors, ["D’Hondt","Sainte-Laguë"], "Differenz Sitze", "Differenz zum Sitzanspruch pro 100 Stimmen", "delta.png", trans=True)
    
    ## MSE Sitze pro 100 Stimmen
    mse_dhondt = sum([mse**2 for mse in data[1]])/len(data[1])
    mse_sainte = sum([mse**2 for mse in data[2]])/len(data[2])
    
    if verbose > 1:
        print("\n## MSE von Sitze pro 100 Stimmen\n")
        output([("D’Hondt", mse_dhondt), ("Sainte-Laguë", mse_sainte)])
    
    if plot:
        tupelplot((mse_dhondt, mse_sainte), ["D’Hondt","Sainte-Laguë"], "MSE von Sitze pro 100 Stimmen", "Fehler pro 100 Stimmen nach Verteilungsverfahren", "fehler.png")


def readwahlfile(filename="wahlergebnis"):
    if not os.path.isfile(filename):
        print("DEBUG: No file found:", filename)
        return None
    wahlfile = open(filename, "r")
    lines = wahlfile.readlines()
    wahlfile.close()
    ergebnis = []
    for line in lines:
        if line.strip():
            raw = [x.strip() for x in line.split(",")]
            ergebnis.append((raw[0], int(raw[1]), raw[2]))
    return ergebnis

"""
    for stew in os.listdir("stews"):
        if stew.endswith(".md") and not stew[:-3] in blacklist:
            stews.append(stew[:-3])
    return random.choice(stews)
"""


if __name__ == "__main__":
    main()

