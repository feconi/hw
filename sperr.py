#!/usr/bin/env python3

def sperr(fraktionen, min_prozent=None, min_stimmen=None):
    # fraktion: [("name", stimmen), ...]
    # min_sitze not implementable because verteilverfahren is unknown
    gesamt = sum([x[1] for x in fraktionen])
    if min_prozent:
        for f in fraktionen:
            if f[1] / gesamt < min_prozent / 100:
                print("kick", f)
    if min_stimmen:
        for f in fraktionen:
            if f[1] < min_stimmen:
                print("kick", f)
    # todo actually kick parties
    return fraktionen


if __name__ == "__main__":
    print("file not executable. import instead")

