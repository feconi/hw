#!/usr/bin/env python3

def verteil(sitze, fraktionen):
    """
    sitze: größe des parlaments
    fraktion: [("name", stimmen), ...]
    """
    gesamtstimmen = sum([s[1] for s in fraktionen])
    parlament = []
    for f in fraktionen:
        # print(f[0], 100/gesamtstimmen*f[1])
        parlament.append((f[0], sitze/gesamtstimmen*f[1]))
    return parlament


if __name__ == "__main__":
    print("file not executable. import instead")

