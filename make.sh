#!/bin/bash

if [ ! -d "reveal.js" ]; then
    echo -e "\e[1m== Cloning reveal.js from github\e[0m"
    git clone https://github.com/hakimel/reveal.js.git
    echo -e "\e[1m== Done\e[0m"
fi

echo -e "\e[1m== Copying files\e[0m"
cp index.html reveal.js/index.html
cp logo.png reveal.js/icon.png
cp -r assets reveal.js/
echo -e "\e[32mNow you can open ./reveal.js/index.html in a browser to view the presentation.\e[0m"
echo -e "\e[1m== Done\e[0m"

