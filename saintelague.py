#!/usr/bin/env python3

def verteil(sitze, fraktionen):
    """
    sitze: größe des parlaments
    fraktion: [("name", stimmen), ...]
    """
    verteilung = []
    gesamtstimmen = 0
    hoechstzahlen = []
    parlament = []
    # berechne alle höchstzahlen
    for f in fraktionen:
        gesamtstimmen += f[1]
        hoechstzahlen.append([])
        for divisor in range(1, sitze+1, 2):
            hoechstzahlen[-1].append(f[1]/divisor)
    # sortiere höchstzahlen
    for i in range(sitze):
        index_max = max(range(len([h[0] for h in hoechstzahlen])), key=[h[0] for h in hoechstzahlen].__getitem__)
        verteilung.append((index_max, fraktionen[index_max][0], hoechstzahlen[index_max][0]))
        hoechstzahlen[index_max].pop(0)
    # print([(x[1], x[2]) for x in verteilung])
    # verteile sitze
    for f in fraktionen:
        fsitze = 0
        for v in verteilung:
            if v[1] == f[0]:
                fsitze += 1
        parlament.append((f[0], fsitze))
    return parlament


if __name__ == "__main__":
    print("file not executable. import instead")

