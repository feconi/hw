#!/usr/bin/env python3

def verteil(sitze, fraktionen):
    """
    sitze: größe des parlaments
    fraktion: [("name", stimmen), ...]
    """
    verteilung = []
    gesamtstimmen = 0
    hoechstzahlen = []
    parlament = []
    # berechne alle höchstzahlen
    # n+n/2n+1
    stimmen_max = max([x[1] for x in fraktionen])
    for f in fraktionen:
        gesamtstimmen += f[1]
        hoechstzahlen.append([])
        for divisor in range(0, sitze):
            if divisor:     # do division not by zero
                hoechstzahlen[-1].append(f[1] / (divisor + divisor/(2*divisor+1)))  # division by n+n/2n+1
                # hoechstzahlen[-1].append(f[1]/divisor)
            elif f[1] > 0:  # hack to sort first hoechstzahlen correctly
                hoechstzahlen[-1].append(stimmen_max + f[1])
    # sortiere höchstzahlen
    for i in range(sitze):
        index_max = max(range(len([h[0] for h in hoechstzahlen])), key=[h[0] for h in hoechstzahlen].__getitem__)
        verteilung.append((index_max, fraktionen[index_max][0], hoechstzahlen[index_max][0]))
        hoechstzahlen[index_max].pop(0)
    # return [(x[1], x[2]) for x in verteilung]
    # verteile sitze
    for f in fraktionen:
        fsitze = 0
        for v in verteilung:
            if v[1] == f[0]:
                fsitze += 1
        parlament.append((f[0], fsitze))
    return parlament


if __name__ == "__main__":
    print("file not executable. import instead")

