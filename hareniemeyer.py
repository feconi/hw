#!/usr/bin/env python3

def verteil(sitze, fraktionen):
    """
    sitze: größe des parlaments
    fraktion: [("name", stimmen), ...]
    """
    gesamtstimmen = sum([s[1] for s in fraktionen])
    quoten = []
    reste = []
    for f in fraktionen:
        quoten.append( f[1]*sitze // gesamtstimmen)
        reste.append(( f[1]*sitze / gesamtstimmen - quoten[-1], len(reste)))
    verteilt = sum(quoten)
    reste.sort(reverse=True)
    for i in range(sitze-verteilt):     # verteile restliche sitze
        quoten[reste[i][1]]+=1
    parlament = []
    for i in range(len(fraktionen)):
        parlament.append((fraktionen[i][0], quoten[i]))
    return parlament


if __name__ == "__main__":
    print("file not executable. import instead")

