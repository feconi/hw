# HW - HondtWechsel

*This project is mostly in german and may be translated in the future.*
*project is work in progress*

This project is for educational use to show that the D'Hondt method is not democratic and never should be used.
A good alternative is the Sainte-Lagu:e method.

## content

0. content
1. pythonscript
2. presentation
3. license

## Sitzverteilungsscript

* main.py
* anspruch.py
* dhondt.py
* saintelague.py
* adams.py

### main.py

This srcipt import the other ones and outputs the results to stdout.
The output can be piped into an markdown file.

**dependecies**
* matplotlib

```
pip install matplotlib
```

**usage**

There will be parameters in the future.

```
main.py
```

If plot is set to True the script generates pie plots showing the proportions and save them in ./assets
These are the images wich are used in the presentation.

### anspruch.py

Calculates the seat claims by simply dividing the sum of all votes by the votes of the party and then multiplying the restult with the size of the parlament.

### dhondt.py

Calculates the D'Hondt proportion using the Höchstzahlverfahren.

### saintelague.py

Calculates the Sainte-Lagu:e proportion using the Höchstzahlverfahren.

### adams.py

Calculates the Adam proportion using the Höchstzahlverfahren.

## Presentation

### Installation

Zur Installation der Präsentation kann **make.sh** verwendet werden.
Das Klonen von reveal.js wird nur initial durchgeführt.
```
./make.sh
```

### Manuelle Installation:

1. Downloade reveal.js
```
git clone https://github.com/hakimel/reveal.js.git
```
2. Ersetze die index.html
```
cp index.html reveal.js/index.html
```
3. Kopiere die assets
```
# mkdir reveal.js/assets
cp assets/* reveal.js/assets/
```

Jetzt kann die Praesentation genutzt werden, indem **reveal.js/index.html** in einem Browser geoeffnet wird.

### Slides

Alle Slides der Praesentation sind in der **index.html** enthalten.
Jede Slide wird durch ein `<section>` repraesentiert.

Fuer Details lies das Readme von reveal.js.
https://github.com/hakimel/reveal.js

### Nutzungsanleitung

Mit den Pfeiltasten kann durch die Praesentation navigiert werden.
Mit Esc kann in eine Slide-overview-Ansicht gewechselt werden.
Mit F kann in den Vollbildmodus gewechselt werden.

Fuer Details schau im Readme von reveal.js nach.
https://github.com/hakimel/reveal.js.git

## License

> ----------------------------------------------------------------------------
> "THE BEER-WARE LICENSE" (Revision 42):
> <feconi@posteo.de> wrote this thing. As long as you retain this notice you
> can do whatever you want with this stuff. If we meet some day, and you think
> this stuff is worth it, you can buy me a beer in return - Asterix
> ----------------------------------------------------------------------------

